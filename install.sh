#! /bin/bash
sourcedir=""

function extractBaseDir
	{
		local scriptname=""
		if [ "x$(uname -s)" == "xDarwin" ]; then
			# test if greadlink is installed
			command -v greadlink 2> /dev/null
			if [ $? -ne 0 ]; then
				echo "greadlink needed on OS X systems. Pease install via MacPorts or Homebrew ..."
				return 1
			fi
			scriptname=`greadlink -f $1`
		else
			scriptname=`readlink -f $1`
		fi
		sourcedir=`dirname $scriptname` 
		return 0
	}
	
userbin=$HOME/bin
extractBaseDir $0
status=$?
if [ $status -ne 0 ]; then 
	echo "Cannot install system on platform $(uname -s)"
	exit 1; 
fi
echo Sourcedir $sourcedir

if [ ! -d $userhome ]; then mkdir -p $userhome; fi

fls=(downloadtraintest legotrain_generate.C generatetraintest runtraintest)
for f in ${fls[@]}; do 
	cp $sourcedir/$f $userbin/; 
	chmod u+rx $userbin/$f
done